
import { SkComponentImpl } from '../../../sk-core/src/impl/sk-component-impl.js';
import { SkRequired } from '../../../sk-form-validator/src/validators/sk-required.js';

export var SW_FORWARDED_ATTRS = {
    'disabled': 'disabled', 'checked': 'checked', 'required': 'required'
};

export class SkSwitchImpl extends SkComponentImpl {

    get suffix() {
        return 'switch';
    }

    get input() {
        return this.comp.el.querySelector('input');
    }

    dumpState() {
        return {
            'contents': this.comp.contentsState
        }
    }

    restoreState(state) {
        super.restoreState(state);
        this.comp._inputEl = null;
        this.forwardAttributes();
        this.bindEvents();
    }

    bindEvents() {
    }

    afterRendered() {
        super.afterRendered();
        this.restoreState({ contentsState: this.contentsState || this.comp.contentsState });
    }

    forwardAttributes() {
        if (this.input) {
            for (let attrName of Object.keys(SW_FORWARDED_ATTRS)) {
                let value = this.comp.getAttribute(attrName);
                if (value) {
                    this.input.setAttribute(attrName, value);
                } else {
                    if (this.comp.hasAttribute(attrName)) {
                        this.input.setAttribute(attrName, '');
                    }
                }
            }
        }
    }

    enable() {
        super.enable();
    }

    disable() {
        super.disable();
    }

    check() {

    }

    uncheck() {

    }

    onClick(event) {
        if (this.comp.getAttribute('disabled')) {
            return false;
        }
        if (! this.comp.getAttribute('checked')) {
            this.comp.setAttribute('checked', 'checked');
        } else {
            this.comp.removeAttribute('checked');
        }
        let result = this.validateWithAttrs();
        if (typeof result === 'string') {
            this.showInvalid();
        } else {
            this.showValid();
        }
    }

    get skValidators() {
        if (! this._skValidators) {
            this._skValidators = {
                'sk-required': new SkRequired(),
            };
        }
        return this._skValidators;
    }

    showInvalid() {
        super.showInvalid();
        this.inputEl.classList.add('form-invalid');
    }

    showValid() {
        super.showValid();
        this.inputEl.classList.remove('form-invalid');
    }

    focus() {
        setTimeout(() => {
            this.comp.inputEl.focus();
        }, 0);
    }
}