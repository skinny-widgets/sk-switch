
import { SkElement } from '../../sk-core/src/sk-element.js';


export class SkSwitch extends SkElement {

    get cnSuffix() {
        return 'switch';
    }

    get impl() {
        if (!this._impl) {
            this.initImpl();
        }
        return this._impl;
    }

    set impl(impl) {
        this._impl = impl;
    }

    get inputEl() {
        if (! this._inputEl && this.el) {
            this._inputEl = this.el.querySelector('input');
        }
        return this._inputEl;
    }

    set inputEl(el) {
        this._inputEl = el;
    }

    get subEls() {
        return [ 'inputEl' ];
    }

    set checked(checked) {
        this.setAttribute('checked', checked);
    }

    get checked() {
        return this.getAttribute('checked');
    }


    set value(value) {
        if (value === null) {
            this.removeAttribute('value');
        } else {
            this.setAttribute('value', value);
        }
    }

    get value() {
        if (this.getAttribute('checked')) {
            return this.getAttribute('value');
        } else {
            return null;
        }
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if (! this.inputEl) {
            return false;
        }
        if (name === 'disabled' && oldValue !== newValue) {
            if (newValue) {
                this.impl.disable();
            } else {
                this.impl.enable();
            }
        }
        if (name === 'checked') {
            if (newValue) {
                this.impl.check();
            } else {
                this.impl.uncheck();
            }
            this.dispatchEvent(new CustomEvent('change', { target: this, detail: { value: this.getAttribute('value'), checked: newValue }, bubbles: true, composed: true }));
            this.dispatchEvent(new CustomEvent('skchange', { target: this, detail: { value: this.getAttribute('value'), checked: newValue }, bubbles: true, composed: true }));
        }
        if (name === 'form-invalid') {
            if (newValue !== null) {
                this.impl.showInvalid();
            } else {
                this.impl.showValid();
            }
        }
        this.callPluginHook('onAttrChange', name, oldValue, newValue);
    }
    get validity() {
        if (this.hasAttribute('required')) {
            if (this.hasAttribute('checked')) {
                return { valid: true, valueMissing: true };
            }
        }
        return { valid: false };
    }

    static get observedAttributes() {
        return [ 'disabled', 'checked', 'form-invalid', 'required' ];
    }

}
