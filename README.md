# Skinny Widgets Switch


switch element

```
npm i sk-switch sk-switch-jquery --save
```

then add the following to your html

```html
<sk-config
    theme="jquery"
    base-path="/node_modules/sk-core/src"
    theme-path="/node_modules/sk-theme-jquery"
></sk-config>
<sk-switch id="mySwitch" checked value="11"></sk-switch>
<script type="module">
    import { SkConfig } from '/node_modules/sk-core/sk-config.js';
    import { SkSwitch } from '/node_modules/sk-switch/sk-switch.js';

    customElements.define('sk-config', SkConfig);
    customElements.define('sk-switch', SkSwitch);
</script>
```

#### slots

**default (not specified)** - draws label for input

**label** - draws label for input

```html
<sk-switch id="mySwitch1">Some Label</sk-switch>
```

#### template

id: SkSwitchTpl